FROM php:7.2.3-apache

RUN apt-get update \
    && apt-get install -y \
        zip \
        unzip \
        vim \
        mysql-client \
        git \
        ssh \
        wget \
        zlib1g-dev \
        libicu-dev \
        libmemcached-dev \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libmcrypt-dev \
        libpng-dev \
        g++ \
    && rm -rf /var/lib/apt/lists/*

# Composer
ARG COMPOSER_VERSION_COMMIT_HASH=1b137f8bf6db3e79a38a5bc45324414a6b1f9df2

ENV COMPOSER_ALLOW_SUPERUSER=1
RUN wget https://raw.githubusercontent.com/composer/getcomposer.org/$COMPOSER_VERSION_COMMIT_HASH/web/installer -O - -q | php -- && \
   mv composer.phar /usr/bin/composer && \
   chmod +x /usr/bin/composer

# PHP configuration
COPY .docker/admin/php.ini /usr/local/etc/php

# Enable PHP extensions
RUN docker-php-ext-configure intl \
   && docker-php-ext-install pdo pdo_mysql opcache intl pcntl zip \
   && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
   && docker-php-ext-install -j$(nproc) gd

# Apache configuration
RUN rm /etc/apache2/sites-available/*.conf
RUN rm /etc/apache2/sites-enabled/*.conf
COPY .docker/admin/app.conf /etc/apache2/sites-available/app.conf
RUN rm /etc/apache2/apache2.conf
COPY .docker/admin/httpd.conf /etc/apache2/apache2.conf
COPY .docker/admin/.htpasswd /etc/htpasswd/.htpasswd
RUN a2ensite app.conf
RUN a2enmod rewrite

WORKDIR /app

ARG SSH_PRIVATE_KEY_PATH=.docker/admin/studi_core
RUN mkdir -p /root/.ssh && \
    chmod 0700 /root/.ssh
COPY ${SSH_PRIVATE_KEY_PATH} /root/.ssh/id_rsa
RUN chmod 600 /root/.ssh/id_rsa
RUN echo "Host bitbucket.org\n\tStrictHostKeyChecking no\n" >> ~/.ssh/config
RUN cat ~/.ssh/config
RUN ssh -v git@bitbucket.org

# Finally, install the app.
COPY . /app/
RUN composer global require hirak/prestissimo
RUN composer install --no-interaction --no-scripts --no-autoloader
RUN cp app/config/parameters.yml.env app/config/parameters.yml
RUN composer dump-autoload --no-dev --classmap-authoritative
RUN chown www-data:www-data -R /app
RUN rm /root/.ssh/id_rsa
COPY .docker/admin/start.sh /usr/local/bin/start
RUN chmod u+x /usr/local/bin/start
RUN chown -R www-data:www-data /app

CMD ["/usr/local/bin/start"]