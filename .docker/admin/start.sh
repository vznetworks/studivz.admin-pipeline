#!/usr/bin/env bash

# This script is used as an entrypoint for API container.

set -e
role=${CONTAINER_ROLE:-app}
env=${SYMFONY_ENV:-prod}

# Here you can do some pre-start tasks
# on each container before start on production.
if [ "$env" != "local" ]; then
	echo "Running Symfony scripts..."
	composer run-script symfony-scripts

	echo "Setting correct ownership..."
    mkdir -p /app/var/sessions/prod
	chown -R www-data:www-data /app/var
fi

if [ "$env" == "prod" ]; then
	echo "Warmup Symfony cache for prod env..."
	php bin/console cache:warmup --env=prod
fi

if [ "$env" == "dev" ]; then
	echo "Warmup Symfony cache for dev env..."
	php bin/console cache:warmup --env=dev
fi

echo "Running app..."
exec apache2-foreground
