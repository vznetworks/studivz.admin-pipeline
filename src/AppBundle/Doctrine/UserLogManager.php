<?php

namespace AppBundle\Doctrine;

use AppBundle\Entity\UserLog;
use Doctrine\ORM\EntityManager;
use StudiVZ\Core\Entity\User;

class UserLogManager extends BaseManager
{

	/**
	 * UserLogManager constructor.
	 * @param EntityManager $em
	 */
	public function __construct(EntityManager $em)
	{
		parent::__construct($em);
	}

	/**
	 * @param UserLog $userLog
	 * @return UserLog
	 * @throws \Exception
	 */
	public function updateUserLog(UserLog $userLog)
	{
		if (!$userLog) {
			throw  new \Exception('User log is not exist');
		}

		$this->em->persist($userLog);
		$this->em->flush();

		return $userLog;
	}

	/**
	 * Create user log
	 *
	 * @param $entityId
	 * @param $entity
	 * @param User $supportMember
	 * @param $action
	 * @param $content
	 * @return UserLog
	 */
	public function createUserLog($entityId, $entity, User $supportMember, $action, $content, $messageSent)
	{
		$userLog = new UserLog();

		$userLog
			->setEntityId($entityId)
			->setEntity($entity)
			->setSupportMember($supportMember)
			->setAction($action)
			->setContent($content)
			->setCreatedAt(new \DateTime())
			->setMessageSent($messageSent)
		;

		$this->updateUserLog($userLog);

		return $userLog;
	}

	/**
	 * Get user log activity for entity
	 *
	 * @param $entityId
	 * @param $entity
	 * @return array
	 */
	public function getUserLogForEntity($entityId, $entity)
	{
		$repository = $this->em->getRepository(UserLog::class);

		return $repository->findBy(['entity' => $entity, 'entityId' => $entityId]);
	}


}