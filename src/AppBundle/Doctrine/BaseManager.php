<?php


namespace AppBundle\Doctrine;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;

class BaseManager extends EntityRepository
{

	/**
	 * @var EntityManager
	 */
	protected $em;

	/**
	 * @var string
	 */
	protected $class;

	/**
	 * Constructor.
	 *
	 * @param EntityManager            $em
	 * @param string                   $class
	 */
	public function __construct(EntityManager $em)
	{
		$this->em = $em;
	}

}