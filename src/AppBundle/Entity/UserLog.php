<?php

namespace AppBundle\Entity;

use StudiVZ\Core\Entity\User;

class UserLog
{
	const EMAIL_CHANGED = 'email_changed';
	const PASSWORD_CHANGED = 'password_changed';
	const PASSWORD_RESET = 'password_reset';
	const BIRTHDAY_CHANGED = 'birthdate_changed';
	const GENDER_CHANGED = 'gender_changed';
	const BYPASS_LOGIN_USED = 'bypass_login_used';
	const USER_BANNED = 'user_banned';
	const USER_DELETED = 'user_deleted';
	const USER_ANONYMIZED = 'user_anonymized';
	const COMMENT_DELETED = 'comment_deleted';
	const THREAD_DELETED = 'thread_deleted';
	const GROUP_DELETED = 'group_deleted';
	const DESCRIPTION_CHANGED = 'description_changed';


	/**
	 * @var integer
	 */
	private $id;

	/**
	 * @var User
	 */
	private $supportMember;

	/**
	 * @var string
	 */
	private $entity;

	/**
	 * @var integer
	 */
	private $entityId;

	/**
	 * @var string
	 */
	private $action;

	/**
	 * @var array
	 */
	private $content;

	/**
	 * @var \DateTime
	 */
	private $createdAt;

	/**
	 * @var boolean
	 */
	private $messageSent;

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param User $supportMember
	 * @return $this
	 */
	public function setSupportMember(User $supportMember)
	{
		$this->supportMember = $supportMember;

		return $this;
	}

	/**
	 * @return User
	 */
	public function getSupportMember(): User
	{
		return $this->supportMember;
	}

	/**
	 * @param string $entity
	 * @return $this
	 */
	public function setEntity(string $entity)
	{
		$this->entity = $entity;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getEntity(): string
	{
		return $this->entity;
	}

	/**
	 * @param int $entityId
	 * @return $this
	 */
	public function setEntityId($entityId)
	{
		$this->entityId = $entityId;

		return $this;
	}

	/**
	 * @return int
	 */
	public function getEntityId()
	{
		return $this->entityId;
	}

	/**
	 * @param string $action
	 * @return $this
	 */
	public function setAction(string $action)
	{
		$this->action = $action;

		return $this;
	}

	/**
	 * @return string
	 */
	public function getAction(): string
	{
		return $this->action;
	}

	/**
	 * @param array $content
	 * @return $this
	 */
	public function setContent($content)
	{
		$this->content = $content;

		return $this;
	}

	/**
	 * @return array
	 */
	public function getContent()
	{
		return $this->content;
	}

	/**
	 * @param \DateTime $createdAt
	 * @return $this
	 */
	public function setCreatedAt(\DateTime $createdAt)
	{
		$this->createdAt = $createdAt;

		return $this;
	}

	/**
	 * @return \DateTime
	 */
	public function getCreatedAt(): \DateTime
	{
		return $this->createdAt;
	}

	/**
	 * @param bool $messageSent
	 * @return $this
	 */
	public function setMessageSent(bool $messageSent)
	{
		$this->messageSent = $messageSent;

		return $this;
	}

	/**
	 * @return bool
	 */
	public function isMessageSent(): bool
	{
		return $this->messageSent;
	}
}