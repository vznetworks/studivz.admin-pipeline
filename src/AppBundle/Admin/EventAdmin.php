<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class EventAdmin extends AbstractAdmin
{

	/**
	 * @return \StudiVZ\Core\Doctrine\EventManager
	 */
	private function getEventManager()
	{
		return $this->getConfigurationPool()->getContainer()->get('studivz_core.event.manager');
	}

	public function updateEvent(\StudiVZ\Core\Entity\Event $event)
	{
		$user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
		$event->setUser($user);
		$this->getEventManager()->updateEvent($event);
	}

	public function removeGroup(\StudiVZ\Core\Entity\Event $event)
	{
		$this->getEventManager()->deleteEventObject($event);
	}

	public function prePersist($object)
	{
		$this->updateEvent($object);
	}

	public function preUpdate($object)
	{
		$this->updateEvent($object);
	}

	public function preRemove($object)
	{
		$this->updateEvent($object);
	}



	protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('entity')
            ->add('entityId')
            ->add('privacy')
            ->add('title')
            ->add('description')
            ->add('dateFrom')
            ->add('dateTo')
            ->add('created')
            ->add('theme')
            ->add('id')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
			->add('id')
			->add('privacy')
            ->add('title')
            ->add('description', 'html', array('truncate' => array('length' => 30)))
            ->add('dateFrom')
            ->add('created')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('entity')
            ->add('entityId')
            ->add('privacy')
            ->add('title')
            ->add('description')
            ->add('dateFrom')
            ->add('dateTo')
            ->add('theme')
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('entity')
            ->add('entityId')
            ->add('privacy')
            ->add('title')
            ->add('description')
            ->add('dateFrom')
            ->add('dateTo')
            ->add('created')
            ->add('theme')
            ->add('id')
        ;
    }
}
