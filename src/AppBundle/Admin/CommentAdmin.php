<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class CommentAdmin extends AbstractAdmin
{
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('content')
            ->add('entity')
            ->add('entityId')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('entity')
            ->add('entityId')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                     'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
    	$content = $this->getSubject()->getContent();

    	if(isset($content['text'])) {
    		$content = $content['text'];
		} else if($content['content']) {
			$content = $content['content'];
		}

        $formMapper
            ->add('content', 'textarea', ['data' => $content])
            ->add('author', 'text', ['disabled' => true])
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('content', 'array')
            ->add('rating')
            ->add('author')
            ->add('entity')
            ->add('entityId')
        ;
    }
}
