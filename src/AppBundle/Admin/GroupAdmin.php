<?php

namespace AppBundle\Admin;

use AppBundle\Doctrine\UserLogManager;
use AppBundle\Entity\UserLog;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use StudiVZ\Core\Doctrine\FileManager;
use StudiVZ\Core\Doctrine\GroupManager;
use StudiVZ\Core\Doctrine\GroupMemberManager;
use StudiVZ\Core\Doctrine\ReportManager;
use StudiVZ\Core\Doctrine\TaxonomyTermManager;
use StudiVZ\Core\Doctrine\TipsManager;
use StudiVZ\Core\Doctrine\UserManager;
use StudiVZ\Core\Entity\Group;
use StudiVZ\Core\Entity\GroupMember;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Sonata\AdminBundle\Model\ModelManagerInterface;

class GroupAdmin extends BaseAdmin
{
	/**
	 * @var UserLogManager
	 */
	private $userLogManager;

	/**
	 * @var GroupManager
	 */
	private $groupManager;

	/**
	 * @var GroupMemberManager
	 */
	private $groupMemberManager;

	/**
	 * @var TipsManager
	 */
	private $tipsManager;

	/**
	 * @var ModelManagerInterface
	 */
	protected $modelManager;

	/**
	 * @var UserManager
	 */
	private $userManager;

	/**
	 * @var ReportManager
	 */
	private $reportManager;

	/** @var FileManager */
	private $fileManager;

	/**
	 * {@inheritdoc}
	 */
	public function __construct(
		string $code,
		string $class,
		string $baseControllerName,
		UserLogManager $userLogManager,
		GroupManager $groupManager,
		TipsManager $tipsManager,
		ModelManagerInterface $modelManager,
		UserManager $userManager,
		ReportManager $reportManager,
        FileManager $fileManager,
        GroupMemberManager $groupMemberManager
	){
		parent::__construct($code, Group::class, 'AppBundle:GroupAdmin');
		$this->userLogManager = $userLogManager;
		$this->groupManager = $groupManager;
		$this->tipsManager = $tipsManager;
		$this->modelManager = $modelManager;
		$this->userManager = $userManager;
		$this->reportManager = $reportManager;
        $this->fileManager = $fileManager;
        $this->groupMemberManager = $groupMemberManager;
	}

	/**
	 * @param string $name
	 * @return mixed|null|string
	 */
	public function getTemplate($name)
	{
		switch ($name) {
			case 'edit':
				return 'AppBundle:admin:custom/group/edit.html.twig';
				break;
			case 'show':
				return 'AppBundle:admin:custom/group/show.html.twig';
				break;
			default:
				return parent::getTemplate($name);
				break;
		}
	}

	public function updateGroup(\StudiVZ\Core\Entity\Group $group)
	{
		if(!$group->getFounder()) $group->setFounder($this->getLoggedUser());
		$this->groupManager->updateGroup($group);

		$isTip = $this->getForm()->get('isTip')->getData();
		if(isset($group->isTip)) {
			$isTip = $group->isTip;
		}

		$this->tipsManager->changeEntityTip(Group::class, $group->getId(), $this->getLoggedUser(), $isTip);
	}

	public function prePersist($object)
	{
		$this->updateGroup($object);
	}

    /**
     * @param Group $object
     */
	public function preUpdate($object)
	{
		$em = $this->getModelManager()->getEntityManager($this->getClass());
		$groupOriginal = $em->getUnitOfWork()->getOriginalEntityData($object);

        $updatedPhotoId = $this->getForm()->get('photoId')->getData();
        $updatedFounderId = $this->getForm()->get('founderId')->getData();
        $currentOwners = $this->groupManager->getGroupAllAdmins($object->getId(), true);
        $currentOwnersIds = [];

        if (is_array($currentOwners)) {
            $currentOwnersIds = array_map(function(GroupMember $value) {
                return $value->getUserId();
            }, $currentOwners);
        }

        $updatedOwnersIds = $this->getForm()->get('ownersIds')->getData();

        if (!$updatedOwnersIds) {
            $updatedOwnersIds = [];
        }

        $ownersToRemove = array_diff($currentOwnersIds, $updatedOwnersIds);

        foreach ($ownersToRemove as $ownerToRemove) {
            $this->groupMemberManager->removeOwner($object->getId(), $ownerToRemove);
        }

        foreach ($updatedOwnersIds as $updatedOwnersId) {
            $this->groupManager->setMemberRole($object->getId(), $updatedOwnersId, GroupMember::RELATION_OWNER);
        }

        $object->setFounder($this->userManager->findById($updatedFounderId));
        if ($updatedPhotoId != null) {
            $object->setPhoto($this->fileManager->findById($updatedPhotoId));
        } else {
            $object->setPhoto(null);
        }

		$this->updateGroup($object);

		if($object->getDescription() != $groupOriginal['description']) {
			$this->userLogManager->createUserLog($object->getId(), Group::class, $this->getLoggedUser(), UserLog::DESCRIPTION_CHANGED, ['old' => $groupOriginal['description'], 'new' => $object->getDescription()], false);
		}
	}

	/**
	 * @param RouteCollection $collection
	 */
	protected function configureRoutes(RouteCollection $collection)
	{
		$collection->add('groupHistory', $this->getRouterIdParameter().'/group-history');
		$collection->add('deleteGroup', $this->getRouterIdParameter().'/delete-group');
		$collection->add('sendOwnerMessage', $this->getRouterIdParameter().'/send-message-to-owner');
		$collection->add('changeOwner', $this->getRouterIdParameter().'/change-group-owner');
	}

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('created', 'doctrine_orm_date_range', array(
				'field_type' => 'sonata_type_date_range_picker',
			))
            ->add('id')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('privacy')
            ->add('type')
            ->add('title', 'html', array('truncate' => array('length' => 30)))
            ->add('description', 'html', array('truncate' => array('length' => 30)))
            ->add('created')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                ),
            ))
            ->add('weight')
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
		$isTip = false;
		$lastActivity = null;
		$timesReported = 0;
        $founder = null;
        $ownerIds = [];

		if($this->getSubject() && $this->getSubject()->getId()) {
			$groupOwners = $this->groupManager->getGroupAllAdmins($this->getSubject()->getId(), true);
			$timesReported = count($this->reportManager->getEntityReport(Group::class, $this->getSubject()->getId()));
			if(empty($groupOwners)) {
				$this->getRequest()->getSession()->getFlashBag()->get("warning");
				$this->getRequest()->getSession()->getFlashBag()->set("warning", "Group doesn't has owner");
			} else {
                $ownerIds = array_map(function(GroupMember $value) {
                    return $value->getUserId();
                }, $groupOwners);
			}

			$isTip = $this->tipsManager->isEntityTip(Group::class, $this->getSubject()->getId());
			$memberships = $this->groupManager->getGroupMembersCount($this->getSubject());
		}

        $photoId = ($this->getSubject()->getPhoto() != null) ? $this->getSubject()->getPhoto()->getId() : '';

		$formMapper
			->add('id', NumberType::class, ['disabled' => true])
			->add('privacy', 'choice', [
                'choices'=>[
					'public' => Group::PRIVACY_PUBLIC,
                	'private'=> Group::PRIVACY_PRIVATE,
                	'closed'=> Group::PRIVACY_CLOSED,
					'friends' => Group::PRIVACY_FRIENDS
				],
            ])
            ->add('type', 'choice', [
                'choices'=>[
                	'Blog'=> Group::TYPE_BLOG,
					'Forum' => Group::TYPE_FORUM,
					'Chat' => Group::TYPE_CHAT,
					'Game' => Group::TYPE_GAME
				],
            ])
			->add('category', null, ['group_by' => function($choiceValue, $key, $value) {
					if($choiceValue->getVocabulary()->getName() == TaxonomyTermManager::CATEGORIES_VOCABULARY_NAME) {
						if (is_null($choiceValue->getParent())) {
							return 'Group Categories';
						} else {
							return $choiceValue->getParent()->getName();
						}
					} else {
						return 'Other Categories';
					}
				}]
			)
            ->add('title')
            ->add('slug')
            ->add('description')
			->add('isTip', 'checkbox', array('mapped' => false, 'required' => false, 'data' => $isTip ? $isTip->getEnabled() : false))
            ->add('weight', 'number')
            ->add('founderId', 'text', ['data' => $this->getSubject()->getFounder()->getId(), 'mapped' => false])
            ->add('ownersIds', 'collection', ['mapped' => false, 'allow_add' => true, 'allow_delete' => true, 'data' => $ownerIds])
			->add('lastView', DateType::class, ['disabled' => true, 'widget' => 'single_text'])
			->add('countMember', NumberType::class, ['disabled' => true, 'mapped' => false, 'label' => 'Number of members', 'data' => isset($memberships['member']) ? $memberships['member'] : 0 ])
			->add('countModerator', NumberType::class, ['disabled' => true, 'mapped' => false, 'label' => 'Number of moderators', 'data' => isset($memberships['moderator']) ? $memberships['moderator'] : 0 ])
			->add('timesReported', NumberType::class, ['disabled' => true, 'mapped' => false, 'label' => 'Times reported', 'data' => $timesReported ])
			->add('created', 'date', ['disabled' => true, 'widget' => 'single_text'])
            ->add('photoId', 'text', ['data' => $photoId, 'mapped' => false, 'required' => false])
		;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('privacy')
            ->add('type')
            ->add('title')
            ->add('slug')
            ->add('description')
            ->add('weight')
            ->add('created')
        ;
    }
}
