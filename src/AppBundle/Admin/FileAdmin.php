<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use StudiVZ\Core\Doctrine\FileManager;

class FileAdmin extends BaseAdmin
{
    /**
	 * @var FileManager
	 */
    private $fileManager;
    
    /**
	 * {@inheritdoc}
	 */
	public function __construct(
		string $code,
		string $class,
		string $baseControllerName,
		FileManager $fileManager
	){
        parent::__construct($code, $class, $baseControllerName);
        
		$this->fileManager = $fileManager;
    }
    
    public function toString($object)
    {
        return $object->getFileName();
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        // to remove a single route
        $collection->remove('edit');
        // OR remove all route except named ones
        // $collection->clearExcept(array('list', 'show'));
    }
    
    public function create($object)
    {
        $file = $object->getUploadedFile();
        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        $object = $this->fileManager->uploadAndStore($file, 'public', 'images', $user);

        return $object;
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id');
    }

    protected function configureListFields(ListMapper $list)
    {
        $list
            ->add('id')
            ->add('resizePhotos', null, [
                'template' => 'AppBundle::admin/list_image_template.html.twig',
                'width' => 100,
                'size' => 'photo_thumb'
                ])
            ->add('disk')
            ->add('path')
            ->add('mimeType')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'delete' => []
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('file', FileType::class, [
                'required' => true
            ])
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('resizePhotos', null, [
                'template' => 'AppBundle::admin/show_image_template.html.twig',
                'label' => 'Preview',
                'width' => 250,
                'size' => 'photo_small',
                'show_link' => true
                ])
            ->add('disk')
            ->add('path')
            ->add('mimeType')
            ->add('size')
            ->add('originalName')
            ->add('originalExtension')
        ;
    }
}