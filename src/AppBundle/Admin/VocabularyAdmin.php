<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Doctrine\ORM\EntityManager;

use SymfonyContrib\Bundle\TaxonomyBundle\Entity\Vocabulary;

class VocabularyAdmin extends AbstractAdmin
{
    public function getClass()
    {
        return Vocabulary::class;
    }

    // /**
    //  * @return EntityManager
    //  */
    // private function getVocabularyManager()
    // {
    //     // return $this->getConfigurationPool()->getContainer()->get('studivz_core.user.manager');
    //     return $this->getConfigurationPool()->getContainer()->get('doctrine')->getRepository(Vocabulary::class);
    // }

    // public function updateVocabulary(Vocabulary $vocabulary)
    // {
    //     $this->getVocabularyManager()->persist($vocabulary);
    //     $this->getVocabularyManager()->flush();
    // }

    // public function removeVocabulary(Vocabulary $vocabulary)
    // {
    //     $this->getVocabularyManager()->remove($vocabulary);
    //     $this->getVocabularyManager()->flush();
    // }

    // public function prePersist($object)
    // {
    //     $this->updateVocabulary($object);
    //     parent::prePersist($object);
    // }

    // public function preUpdate($object)
    // {
    //     $this->updateVocabulary($object);
    //     parent::preUpdate($object);
    // }

    // public function preRemove($object)
    // {
    //     $this->removeVocabulary($object);
    // }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('label')
            ->add('name')
            ->add('desc')
            ->add('orderable')
            ->add('weight')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('label')
            ->add('name')
            ->add('desc')
            ->add('orderable')
            ->add('weight')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('label')
            ->add('name')
            ->add('desc')
            ->add('orderable')
            ->add('weight')
            // ->add('createdAt')
            // ->add('updatedAt')
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('label')
            ->add('name')
            ->add('desc')
            ->add('orderable')
            ->add('weight')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }
}
