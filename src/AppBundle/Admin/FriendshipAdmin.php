<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use StudiVZ\Core\Entity\Friendship;

class FriendshipAdmin extends BaseAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('status')
            ->add('fromUserId')
            ->add('toUserId')
            ->add('created')
            ->add('message')
            ->add('id')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('status')
            ->add('fromUserId')
            ->add('toUserId')
            ->add('created')
            ->add('message')
            ->add('id')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                ),
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('status', 'choice', [
                'choices'=>['Waiting'=> Friendship::STATUS_WAITING, 'Accepted' => Friendship::STATUS_ACCEPTED, 'Rejected' => Friendship::STATUS_REJECTED],
            ])
            ->add('fromUserId')
            ->add('toUserId')
            ->add('message')
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('status')
            ->add('fromUserId')
            ->add('toUserId')
            ->add('created')
            ->add('message')
            ->add('id')
        ;
    }
}
