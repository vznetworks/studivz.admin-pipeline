<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Form\Type\CollectionType;
use SymfonyContrib\Bundle\TaxonomyBundle\Entity\Term;
use StudiVZ\Core\Entity\User;
use StudiVZ\Core\Entity\EntityReport;
use Sonata\AdminBundle\Route\RouteCollection;

class EntityReportAdmin extends BaseAdmin
{
    protected $baseRoutePattern = 'entityreport';
    protected $baseRouteName = 'entityreport';

    /**
     * {@inheritdoc}
     */
    public function __construct(string $code, string $class, string $baseControllerName)
    {
        parent::__construct($code, EntityReport::class, 'AppBundle:EntityReportAdmin');
    }

    // protected function configureRoutes(RouteCollection $collection)
    // {
    //     $collection->add('show', $this->getRouterIdParameter().'/show');
    // }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('description')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id', null, ['route' => ['name' => 'show']])
            ->add('author',  null, [
                'template' => 'AppBundle::admin/custom/entityreport/list_report_author.html.twig'
            ])
            ->add('reportEntityUrl', 'string', [
                'template' => 'AppBundle::admin/custom/entityreport/list_report_entity.html.twig'
            ])
            ->add('category')
            ->add('description')
            ->add('created')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('reportEntity')
            ->add('reportEntityId')
            ->add('description')
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('author')
            ->add('reportEntity')
            ->add('reportEntityId')
            ->add('reportEntityUrl', 'url', [
                'attributes' => ['target' => '_blank']
            ])
            ->add('description')
            ->add('created')
            ->add('_action', null, [
                'actions' => [
                    'delete' => [],
                ],
            ])
        ;
    }

}
