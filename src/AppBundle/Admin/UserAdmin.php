<?php

namespace AppBundle\Admin;

use AppBundle\Doctrine\UserLogManager;
use AppBundle\Entity\UserLog;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use FOS\UserBundle\Util\PasswordUpdater;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Model\ModelManagerInterface;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Validator\ErrorElement;
use StudiVZ\Core\Doctrine\FriendshipManager;
use StudiVZ\Core\Doctrine\GroupManager;
use StudiVZ\Core\Doctrine\ReportManager;
use StudiVZ\Core\Doctrine\UserFailedLoginManager;
use StudiVZ\Core\Doctrine\UserManager;
use StudiVZ\Core\Entity\User;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Knp\Menu\ItemInterface as MenuItemInterface;

class UserAdmin extends BaseAdmin
{

	/**
	 * @var UserLogManager
	 */
	private $userLogManager;

	/**
	 * @var UserFailedLoginManager
	 */
	private $userFailedLoginManager;

	/**
	 * @var FriendshipManager
	 */
	private $friendshipManager;

	/**
	 * @var UserManager
	 */
	private $userManager;

	/**
	 * @var GroupManager
	 */
	private $groupManager;

	/**
	 * @var TokenStorageInterface
	 */
	private $tokenStorage;

	/**
	 * @var EntityManager
	 */
	private $entityManager;

	/**
	 * @var ReportManager
	 */
	private $reportManager;

	/**
	 * {@inheritdoc}
	 */
	public function __construct(
		string $code,
		string $class,
		string $baseControllerName,
		UserLogManager $userLogManager,
		UserFailedLoginManager $userFailedLoginManager,
		FriendshipManager $friendshipManager,
		UserManager $userManager,
		GroupManager $groupManager,
		TokenStorageInterface $tokenStorage,
		ModelManagerInterface $modelManager,
		EntityManagerInterface $entityManager,
		ReportManager $reportManager
	) {
		parent::__construct($code, User::class, 'AppBundle:UserAdmin');
		$this->userLogManager = $userLogManager;
		$this->userFailedLoginManager = $userFailedLoginManager;
		$this->friendshipManager = $friendshipManager;
		$this->userManager = $userManager;
		$this->groupManager = $groupManager;
		$this->tokenStorage = $tokenStorage;
		$this->entityManager = $entityManager;
		$this->reportManager = $reportManager;

		//		$this->setModelManager($modelManager);
		//		$this->setManagerType(User::class);
	}

	/**
	 * @param string $name
	 * @return mixed|null|string
	 */
	public function getTemplate($name)
	{
		switch ($name) {
			case 'edit':
				return 'AppBundle:admin:custom/user/edit.html.twig';
				break;
			default:
				return parent::getTemplate($name);
				break;
		}
	}

	public function configureTabMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
	{
		parent::configureTabMenu($menu, $action, $childAdmin);

		if ($this->getLoggedUser()->hasRole('ROLE_SUPER_ADMIN')) {
			$menu->addChild('Access', ['uri' => $this->generateUrl('listSupportMembers')]);
		}

		$menu->addChild('Post urgent message to All users', ['uri' => $this->generateUrl('sendUrgentMessage')]);
	}

	public function preUpdate($object)
	{
		$em = $this->getModelManager()->getEntityManager($this->getClass());
		$userOriginal = $em->getUnitOfWork()->getOriginalEntityData($object);

		if ($object->getEmail() != $userOriginal['email']) {
			$this->userManager->updateUserEmail($object);
			$this->userLogManager->createUserLog($object->getId(), User::class, $this->getLoggedUser(), UserLog::EMAIL_CHANGED, ['old' => $userOriginal['email'], 'new' => $object->getEmail()], true);
		}
		if ($object->getBirthday() != $userOriginal['birthday']) {
			$this->userLogManager->createUserLog($object->getId(), User::class, $this->getLoggedUser(), UserLog::BIRTHDAY_CHANGED, ['old' => $userOriginal['birthday'], 'new' => $object->getBirthday()], false);
		}
		if ($object->getGender() != $userOriginal['gender']) {
			$this->userLogManager->createUserLog($object->getId(), User::class, $this->getLoggedUser(), UserLog::GENDER_CHANGED, ['old' => $userOriginal['gender'], 'new' => $object->getGender()], false);
		}
	}

	/**
	 * {@inheritdoc}
	 */
	public function validate(ErrorElement $errorElement, $object)
	{
		//
		$user = $this->userManager->findUserByEmail($object->getEmail());
		if (!is_null($user) && $user->getId() != $object->getId()) {
			$error = 'An error has occurred. The E-mail you want to change does exist already for an other user.';
			$errorElement->with('email')->addViolation($error)->end();
		}
	}

	/**
	 * @param RouteCollection $collection
	 */
	protected function configureRoutes(RouteCollection $collection)
	{
		$collection->add('bypass', $this->getRouterIdParameter() . '/by-pass-login');
		$collection->add('sendUserMessage', $this->getRouterIdParameter() . '/send-message-to-user');
		$collection->add('userAddToImportQueue', $this->getRouterIdParameter() . '/add-to-import-queue');
		$collection->add('userHistory', $this->getRouterIdParameter() . '/user-history');
		$collection->add('resetPassword', $this->getRouterIdParameter() . '/reset-password');
		$collection->add('changePassword', $this->getRouterIdParameter() . '/change-password');
		$collection->add('banUser', $this->getRouterIdParameter() . '/ban-user');
		$collection->add('deleteUser', $this->getRouterIdParameter() . '/delete-user');
		$collection->add('userComments', $this->getRouterIdParameter() . '/list-of-last-comments');
		$collection->add('sendUrgentMessage', 'send-urgent-message');
		$collection->add('deleteComment', 'delete-comment');
		$collection->add('userFiles', $this->getRouterIdParameter() . '/list-of-uploaded-content');
		$collection->add('deleteFile', 'delete-file');
		$collection->add('toggleRevenueAllowed', $this->getRouterIdParameter() . '/toggle-revenue-allowed');

		$collection->add('makeSupportMember', $this->getRouterIdParameter() . '/make-support-member');
		$collection->add('makeSupportAdmin', $this->getRouterIdParameter() . '/make-support-admin');
		$collection->add('listSupportMembers', 'list-of-all-support-members');

		$collection->remove('create');
	}

	/**
	 * @param DatagridMapper $datagridMapper
	 */
	protected function configureDatagridFilters(DatagridMapper $datagridMapper)
	{
		$datagridMapper
			->add('firstname')
			->add('lastname')
			->add('email')
			->add('id')
			->add('birthday', 'doctrine_orm_date_range', array(
				'field_type' => 'sonata_type_date_range_picker',
			))
			->add(
				'gender',
				'doctrine_orm_choice',
				array(
					'label' => 'Gender'
				),
				'choice',
				array(
					'choices' => array(
						'Neutral' => 'n',
						'Female' => 'f',
						'Male' => 'm',
					),
					'expanded' => true,
					'multiple' => true
				)
			)
			->add('registered', 'doctrine_orm_date_range', array(
				'field_type' => 'sonata_type_date_range_picker',
			))
			->add('revenueAllowed');
	}

	/**
	 * @param ListMapper $listMapper
	 */
	protected function configureListFields(ListMapper $listMapper)
	{
		$listMapper
			->add('id')
			->add('firstname', 'html', array('truncate' => array('length' => 10)))
			->add('lastname', 'html', array('truncate' => array('length' => 10)))
			->add('email')
			->add('enabled', null, ['label' => 'Email verified'])
			->add('lastLogin')
			->add('roles')
			->add('gender')
			->add('_action', null, array(
				'actions' => array(
					'show' => array(),
					'edit' => array(),
				)
			));
	}

	/**
	 * @param FormMapper $formMapper
	 */
	protected function configureFormFields(FormMapper $formMapper)
	{
		$memberships = $this->groupManager->getUserGroupMembershipsCount($this->getSubject());
		$numberFailedLogin = $this->userFailedLoginManager->getCountUserFailedLogin($this->getSubject());
		$numberFriends = $this->friendshipManager->getCountMyFriends($this->getSubject());
		$numberTimesReported = $this->reportManager->getEntityReport(User::class, $this->getSubject()->getId());
		$numberUserGames = $this->groupManager->getUserGameCount($this->getSubject());

		$formMapper
			->tab('General')
			->with('User main information', array('class' => 'col-md-4'))
			->add('email', null, array(
				'error_bubbling' => true,
			))
			->add('emailActivatedAt',  'date', ['disabled' => true, 'label' => 'Last change of email'])
			->add('enabled', CheckboxType::class, ['label' => 'Email verified', 'required' => false])
			->add('birthday', 'birthday', array('years' => range(1920, date('Y'))))
			->add('gender', 'choice', [
				'choices' => ['Male' => 'm', 'Female' => 'f', 'Neutral' => 'n'],
			])
			->add('id', NumberType::class, ['disabled' => true])
			->add('firstname')
			->add('lastname')
			->add('description')
			->end()
			->with('User additional information', array('class' => 'col-md-4'))
			->add('registered', 'date', array('disabled' => true))
			->add('lastLogin', 'date', array('disabled' => true))
			->add('failedLogin', NumberType::class, ['disabled' => true, 'mapped' => false, 'label' => 'Number of failed logins', 'data' => isset($numberFailedLogin) ? $numberFailedLogin : 0])
			->add('revenueTotalScore', NumberType::class, ['disabled' => true, 'required' => false])
			//					->add('roles')
			->end()
			->with('User number information', array('class' => 'col-md-4'))
			->add('countMember', NumberType::class, ['disabled' => true, 'mapped' => false, 'label' => 'Number of groups (member)', 'data' => isset($memberships['member']) ? $memberships['member'] : 0])
			->add('countModerator', NumberType::class, ['disabled' => true, 'mapped' => false, 'label' => 'Number of groups (moderator)', 'data' => isset($memberships['moderator']) ? $memberships['moderator'] : 0])
			->add('countOwner', NumberType::class, ['disabled' => true, 'mapped' => false, 'label' => 'Number of groups (owner)', 'data' => isset($memberships['owner']) ? $memberships['owner'] : 0])
			->add('numberFriends', NumberType::class, ['disabled' => true, 'mapped' => false, 'label' => 'Number of friends', 'data' => $numberFriends])
			->add('numberGames', NumberType::class, ['disabled' => true, 'mapped' => false, 'label' => 'Number of games', 'data' => $numberUserGames])
			->add('timesReported', NumberType::class, ['disabled' => true, 'mapped' => false, 'data' => count($numberTimesReported)])
			->end()
			->end();
	}

	/**
	 * @param ShowMapper $showMapper
	 */
	protected function configureShowFields(ShowMapper $showMapper)
	{
		$showMapper
			->add('id')
			->add('email')
			->add('emailActivatedAt', null, ['label' => 'Last change of email'])
			->add('enabled', null, ['label' => 'Email verified'])
			->add('lastLogin')
			->add('roles')
			->add('firstname')
			->add('lastname')
			->add('gender')
			->add('birthday')
			->add('registered')
			->add('revenueTotalScore', NumberType::class, ['disabled' => true, 'required' => false])
			->add('description')
			->add("exportRequested", null, ['template' => 'AppBundle::admin/custom/user/show_export_processing.html.twig'])
			->add("exportFinished", null, ['template' => 'AppBundle::admin/custom/user/show_export_finished.html.twig'])
			->add("exportRequestedDate", null, ['template' => 'AppBundle::admin/custom/user/show_export_requested_date.html.twig']);
	}
}
