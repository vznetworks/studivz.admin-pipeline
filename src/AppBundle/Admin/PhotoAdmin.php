<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class PhotoAdmin extends BaseAdmin
{

	/**
	 * @return \StudiVZ\Core\Doctrine\PhotoManager
	 */
	private function getPhotoManager()
	{
		return $this->getConfigurationPool()->getContainer()->get('studivz_core.photo.manager');
	}

	/**
	 * @param \StudiVZ\Core\Entity\Photo $photo
	 */
	public function removePhoto(\StudiVZ\Core\Entity\Photo $photo)
	{
		$this->getPhotoManager()->deletePhoto($photo->getId());
	}

	/**
	 * @param object $object
	 */
	public function preRemove($object)
	{
		$this->removePhoto($object);
	}

	protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
			->add('id')
			->add('awsId')
            ->add('path')
            ->add('created')
            ->add('filemime')
            ->add('filesize')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
			->add('id')
			->add('awsId')
            ->add('path')
            ->add('created')
//            ->add('filemime')
//            ->add('filesize')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('awsId')
            ->add('path')
            ->add('created')
            ->add('filemime')
            ->add('filesize')
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('awsId')
            ->add('path', NULL, array(
				'template' => 'AppBundle:admin:admin_image_show.html.twig'
			))
            ->add('created')
            ->add('filemime')
            ->add('filesize')
            ->add('id')
        ;
    }
}
