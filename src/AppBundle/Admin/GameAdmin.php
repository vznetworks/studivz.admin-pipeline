<?php


namespace AppBundle\Admin;

use Doctrine\DBAL\Types\DecimalType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\AdminType;
use Sonata\AdminBundle\Show\ShowMapper;

use StudiVZ\Core\Doctrine\CommentManager;
use StudiVZ\Core\Doctrine\RatingManager;
use StudiVZ\Core\Doctrine\ThreadManager;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

use Symfony\Component\Form\Extension\Core\Type\NumberType;

class GameAdmin extends BaseAdmin
{
    /**
     * @var GroupAdmin
     */
    private $groupAdmin;

    /**
     * @var GameOAuthAdmin
     */
    private $gameOAuthAdmin;

    /**
     * @var CommentManager
     */
    private $commentManager;

    /**
     * @var ThreadManager
     */
    private $threadManager;

    /**
     * @var RatingManager
     */
    private $ratingManager;


    public function __construct($code, $class, $baseControllerName, GroupAdmin $groupAdmin, GameOAuthAdmin $gameOAuthAdmin, CommentManager $commentManager, ThreadManager $threadManager, RatingManager $ratingManager)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->groupAdmin = $groupAdmin;
        $this->gameOAuthAdmin = $gameOAuthAdmin;
        $this->commentManager = $commentManager;
        $this->threadManager = $threadManager;
        $this->ratingManager = $ratingManager;
    }

    public function toString($object)
    {
        return $object->getGroup()->getTitle();
    }

    private function updateGame($game)
    {
        $game->getGroup()->isTip = $this->getForm()->get('group')->get('isTip')->getData();
        $this->groupAdmin->updateGroup($game->getGroup());

        $rating = $this->ratingManager->findOrCreate($game->getGroup());
        $rating->setOverallRating($this->getForm()->get('rating')->getData());
        $this->ratingManager->updateEntityRating($rating);
    }

    /**
     * @param object $object
     */
    public function prePersist($object)
    {
        $this->updateGame($object);
    }

    /**
     * @param object $object
     */
    public function preUpdate($object)
    {
        $this->updateGame($object);
    }

    /**
     * @param object $object
     */
    public function postPersist($object)
    {
        $this->createThread($object);
    }

    /**
     * @param object $object
     */
    public function postUpdate($object)
    {
        if (!$this->threadManager->getThreadForEntity($object)) {
            $this->createThread($object);
        }
    }

    /**
     * Create thread for game
     * @param $object
     */
    private function createThread($object)
    {
        $comment = $this->commentManager->create('...', $this->getLoggedUser());
        $this->threadManager->createThread($comment, $object);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('gameId')
            ->add('group.title')
            ->add('gadgetPath')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ]);
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $ratingOverall = 0;
        if (!is_null($this->getSubject()->getGroup())) {
            $rating = $this->ratingManager->findOrCreate($this->getSubject()->getGroup());
            $ratingOverall = $rating->getOverallRating();
        }



        $formMapper
            ->tab('General')
            ->with('General')
            ->add('group', AdminType::class, ['label' => false])
            ->end()
            ->end()
            ->tab('Game')
            ->with('Game')
            ->add('oldId')
            ->add('gadgetPath')
            ->add('rating', 'integer', ['mapped' => false, 'attr' => ['min' => 0, 'max' => 5], 'data' => $ratingOverall])
            // ->add('isMobile', 'checkbox', ['data', false])
            ->add('isMobile', CheckboxType::class, ['label' => 'Game is available on mobile', 'required' => false])
            ->add('gameTutorial', CKEditorType::class)
            ->end()
            ->end()
            ->tab('Game OAuth')
            ->with('Game OAuth')
            ->add('gameOAuth', AdminType::class, ['label' => false])
            ->end()
            ->end();
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $groupFields = $this->groupAdmin->getShowFieldDescriptions();
        $showMapper->with('General');
        foreach ($groupFields as $field => $description) {
            $showMapper->add('group.' . $field, null, ['label' => $field]);
        }
        $showMapper->end();

        $showMapper
            ->with('Game')
            ->add('gameId')
            ->add('gadgetPath')
            ->end();

        $gameOAuthFields = $this->gameOAuthAdmin->getShowFieldDescriptions();
        $showMapper->with('Game OAuth');
        foreach ($gameOAuthFields as $field => $description) {
            $showMapper->add('gameOAuth.' . $field, null, ['label' => $field]);
        }
        $showMapper->end();
    }
}
