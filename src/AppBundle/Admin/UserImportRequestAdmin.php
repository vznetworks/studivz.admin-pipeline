<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use StudiVZ\Core\Entity\Import\UserImportRequest;

class UserImportRequestAdmin extends AbstractAdmin
{
	public function __construct($code, $class, $baseControllerName)
	{
		parent::__construct($code, UserImportRequest::class, $baseControllerName);
	}

	protected function configureRoutes(RouteCollection $collection)
	{
		$collection->remove('create');
	}

	protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('newEmail')
            ->add('oldAccount')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper

			->add('user')
            ->add('newEmail')
            ->add('oldAccount')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('newEmail')
            ->add('oldAccount')
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('newEmail')
            ->add('oldAccount')
			->add('identityCardFront.resizePhotos', null, [
				'template' => 'AppBundle::admin/show_image_template.html.twig',
				'label' => 'Identity card front',
				'width' => 250,
				'size' => 'photo_small',
				'show_link' => true
			])
			->add('identityCardBack.resizePhotos', null, [
				'template' => 'AppBundle::admin/show_image_template.html.twig',
				'label' => 'Identity card back',
				'width' => 250,
				'size' => 'photo_small',
				'show_link' => true
			])
        ;
    }
}
