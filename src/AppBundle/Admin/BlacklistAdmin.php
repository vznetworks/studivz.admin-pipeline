<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Route\RouteCollection;

class BlacklistAdmin extends BaseAdmin
{
    protected $baseRoutePattern = 'blacklist';
    protected $baseRouteName = 'blacklist';
    
    /**
     * {@inheritdoc}
     */
    public function __construct(string $code, string $class, string $baseControllerName)
    {
        parent::__construct($code, null, 'AppBundle:BlacklistAdmin');
    }


    protected function configureRoutes(RouteCollection $collection)
    {
        // $collection->add('clone', $this->getRouterIdParameter().'/clone');
        $collection->clearExcept(['list']);
        $collection->add('import');
    }
}