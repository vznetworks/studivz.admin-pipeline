<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

use SymfonyContrib\Bundle\TaxonomyBundle\Entity\Term;

class TermAdmin extends AbstractAdmin
{
    public function getClass()
    {
        return Term::class;
    }

    public function toString($object)
    {
        return $object->getName();
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('name')
            ->add('vocabulary')
            ->add('enabled')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('name')
            ->add('vocabulary')
            ->add('weight')
            ->add('level')
            ->add('enabled')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('vocabulary', null, ['required' => true])
            ->add('name')
            ->add('desc', null, ['required' => false])
            ->add('weight')
            ->add('path')
            ->add('level')
            ->add('enabled')
			->add('parent')
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('name')
            ->add('desc')
            ->add('weight')
            ->add('path')
            ->add('level')
            ->add('enabled')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }
}
