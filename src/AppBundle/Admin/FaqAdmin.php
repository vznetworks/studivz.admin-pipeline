<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\CoreBundle\Form\Type\CollectionType;
use SymfonyContrib\Bundle\TaxonomyBundle\Entity\Term;

use StudiVZ\Core\Doctrine\FaqManager;

class FaqAdmin extends AbstractAdmin
{

    public function toString($object)
    {
        return $object->getTitle();
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $rep = $this->getConfigurationPool()->getContainer()->get('doctrine')->getRepository('TaxonomyBundle:Term');
        $cats = $rep->getFlatTree(FaqManager::CATEGORIES_VOCABULARY_NAME);
        $catChoices = array();
        $strPrefix = '--------';
        foreach($cats as $cat) {
            $cat->choiceName = substr($strPrefix, 0, $cat->getLevel()) . $cat->getName();
            $catChoices[$cat->getId()] = $cat;
        }

        $datagridMapper
            ->add('title')
            ->add('content')
            ->add('category', 'doctrine_orm_choice', [], 'choice', ['choices' => $catChoices, 'choice_label' => 'choiceName',])
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('title')
            ->add('content')
            ->add('category')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $rep = $this->getConfigurationPool()->getContainer()->get('doctrine')->getRepository('TaxonomyBundle:Term');
        $cats = $rep->getFlatTree(FaqManager::CATEGORIES_VOCABULARY_NAME);
        $catChoices = array();
        $strPrefix = '--------';
        foreach($cats as $cat) {
            $cat->choiceName = substr($strPrefix, 0, $cat->getLevel()) . $cat->getName();
            $catChoices[$cat->getId()] = $cat;
        }

        $formMapper
            ->add('title')
            ->add('content')
            ->add('category', null, [
                'placeholder' => 'Choose an option',
                'choices' => $catChoices,
                'choice_label' => 'choiceName',
                'required' => true
            ])
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('title')
            ->add('content')
            ->add('category')
        ;
    }
}
