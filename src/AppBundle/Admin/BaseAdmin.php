<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;

class BaseAdmin extends AbstractAdmin
{

	/**
	 * @return mixed
	 */
	protected function getLoggedUser()
	{
		return $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
	}

}