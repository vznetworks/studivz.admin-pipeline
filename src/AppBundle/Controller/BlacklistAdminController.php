<?php

namespace AppBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\BlacklistType;
use Symfony\Component\Form\FormInterface;
use StudiVZ\Core\Search\BlacklistClient;
use Elastica\Exception\Connection\HttpException;

class BlacklistAdminController extends CRUDController
{
    const TYPE_USER = 'user';
    const TYPE_GROUP = 'group';
    const TYPE_IP = 'ip';
    const TYPE_PASSWORD = 'password';
    const TYPE_EMAIL = 'email';
    const TYPE_FULLNAME = 'fullname';
    const ELASTICA_INDEXES = [
        self::TYPE_USER => BlacklistClient::INDEX_BLACKLIST_USER,
        self::TYPE_GROUP => BlacklistClient::INDEX_BLACKLIST_GROUP,
        self::TYPE_IP => BlacklistClient::INDEX_BLACKLIST_IP,
        self::TYPE_PASSWORD => BlacklistClient::INDEX_BLACKLIST_PASSWORD,
        self::TYPE_EMAIL => BlacklistClient::INDEX_BLACKLIST_EMAIL,
        self::TYPE_FULLNAME => BlacklistClient::INDEX_BLACKLIST_FULLNAME
    ];
    const WORDS_DELIMITER = "\r\n";

    /**
     * @var BlacklistClient
     */
    private $blacklistClient;

    public function __construct(BlacklistClient $blacklistClient)
    {
        $this->blacklistClient = $blacklistClient;
    }

    /**
     * Returns index manager
     *
     * @return IndexManager
     */
    private function elasticaManager()
    {
        return $this->get('fos_elastica.index_manager');
    }

    public function listAction()
    {
        $request = $this->getRequest();

        $type = $request->get('type', false);
        $content = 'Choose type from menu';
        if ($type !== false) {
            $content = $this->_processForm($request, $type);
        }

        return $this->renderWithExtraParams('AppBundle::admin/custom/blacklist/list.html.twig', [
            'content' => $content,
        ]);
    }

    public function importAction()
    {
        die('tu');
    }

    private function _processForm(Request $request, string $type)
    {   
        $form = $this->createForm(BlacklistType::class, $this->_loadData($request, $type));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = explode(self::WORDS_DELIMITER, $form->getData()['keywords']);

            $this->_saveData($type, $data);
        }

        return $this->_renderForm($form);
    }

    private function _renderForm(FormInterface $form, string $template = 'AppBundle::admin/custom/blacklist/form.html.twig')
    {
        return $this->renderView($template, [
            'form' => $form->createView()
        ]);
    }

    private function _loadData(Request $request, string $type)
    {
        if ($request->isMethod($request::METHOD_GET)) {
            $keywords = '';
            try {
                $this->blacklistClient->loadIndex(self::ELASTICA_INDEXES[$type]);
                $data = $this->blacklistClient->loadData();
                $keywords = implode(self::WORDS_DELIMITER, $data);
            } catch (HttpException $e) {
                $keywords = 'Elastica server is down!!!';
            }

            return ['keywords' => $keywords];
        }

        return null;
    }

    private function _saveData(string $type, array $data)
    {
        $this->blacklistClient->loadIndex(self::ELASTICA_INDEXES[$type]);
        $this->blacklistClient->saveData($data);
    }
}
