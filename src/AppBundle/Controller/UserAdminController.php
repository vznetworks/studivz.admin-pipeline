<?php

namespace AppBundle\Controller;

use AppBundle\Doctrine\UserLogManager;
use AppBundle\Entity\UserLog;
use AppBundle\Form\UserChangePasswordType;
use AppBundle\Form\UserAddToImportQueueType;
use AppBundle\Form\UserDeleteType;
use AppBundle\Form\UserMessageType;
use AppBundle\Form\UserUrgentMessageType;
use StudiVZ\Core\Doctrine\AccountManager;
use StudiVZ\Core\Doctrine\CommentManager;
use StudiVZ\Core\Doctrine\ExportManager;
use StudiVZ\Core\Doctrine\FaqManager;
use StudiVZ\Core\Doctrine\FileManager;
use StudiVZ\Core\Doctrine\ImportManager;
use StudiVZ\Core\Doctrine\ReportManager;
use StudiVZ\Core\Doctrine\ThreadManager;
use StudiVZ\Core\Doctrine\UserBypassManager;
use StudiVZ\Core\Doctrine\UserManager;
use StudiVZ\Core\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Sonata\DatagridBundle\Pager\Doctrine\Pager;
use Sonata\DatagridBundle\ProxyQuery\Doctrine\ProxyQuery;
use Symfony\Component\HttpFoundation\Response;


class UserAdminController extends BaseController
{
	const ROLE_SUPER_ADMIM = 'ROLE_SUPER_ADMIN';
	const ROLE_SUPPORT_MEMBER = 'ROLE_SUPPORT_MEMBER';

	/**
	 * @var UserBypassManager
	 */
	private $userBypassManager;

	/**
	 * @var AccountManager
	 */
	private $accountManager;

	/**
	 * @var ReportManager
	 */
	private $reportManager;

	/**
	 * @var FaqManager
	 */
	private $faqManager;

	/**
	 * @var CommentManager
	 */
	private $commentManager;

	/**
	 * @var ThreadManager
	 */
	private $threadManager;

	/**
	 * @var FileManager
	 */
	private $fileManager;

	/**
	 * @var ExportManager
	 */
	private $exportManager;

	public function __construct(
		UserManager $userManager,
		UserLogManager $userLogManager,
		TokenStorageInterface $tokenStorage,
		UserBypassManager $userBypassManager,
		AccountManager $accountManager,
		ReportManager $reportManager,
		FaqManager $faqManager,
		CommentManager $commentManager,
		ThreadManager $threadManager,
		FileManager $fileManager,
		ExportManager $exportManager,
		ImportManager $importManager
	) {
		parent::__construct($userManager, $userLogManager, $tokenStorage);
		$this->userBypassManager = $userBypassManager;
		$this->accountManager = $accountManager;
		$this->reportManager = $reportManager;
		$this->faqManager = $faqManager;
		$this->commentManager = $commentManager;
		$this->threadManager = $threadManager;
		$this->fileManager = $fileManager;
		$this->exportManager = $exportManager;
		$this->importManager = $importManager;
	}

	/**
	 * Generate By pass login
	 * @param $id
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function bypassAction($id)
	{
		$user = $this->userManager->findById($id);

		$generatePassword = bin2hex(random_bytes(4));

		$userBypass = $this->userBypassManager->createUserBypass($this->getLoggedUser(), $user->getEmail(), $generatePassword);

		$this->userLogManager->createUserLog($user->getId(), User::class, $this->getLoggedUser(), UserLog::BYPASS_LOGIN_USED, [], false);

		$userBypass->setPassword($generatePassword);

		return $this->renderWithExtraParams('AppBundle::admin/custom/user/bypass_show.html.twig', [
			'object' => $userBypass,
		]);
	}

	/**
	 * Generate reset password
	 * @param $id
	 * @return RedirectResponse
	 */
	public function resetPasswordAction($id)
	{
		$user = $this->userManager->findById($id);

		$randomPass = bin2hex(random_bytes(4));
		$user->setPlainPassword($randomPass);
		$user->setConfirmationToken($this->get('fos_user.util.token_generator')->generateToken());
		$user->setPasswordRequestedAt(new \DateTime('+7 days'));
		$this->userManager->updatePassword($user);
		$this->userManager->updateUser($user);

		$this->accountManager->sendPasswordReset($user);
		$this->userLogManager->createUserLog($user->getId(), User::class, $this->getLoggedUser(), UserLog::PASSWORD_RESET, [], true);


		$this->addFlash('sonata_flash_success', 'You have reset the password for ' . $user->getEmail() . '! The user will receive a passwort reset mail an can create a new password.');

		return $this->redirectToEdit($id);
	}

	/**
	 * Add to import queue action
	 * @param Request $request
	 * @return \Symfony\Component\HttpFoundation\Response
	 * @internal param $id
	 */
	public function userAddToImportQueueAction(Request $request, $id)
	{
		$user = $this->userManager->findById($id);
		$form = $this->createForm(userAddToImportQueueType::class);
		$form->handleRequest($request);

		$didImport = $this->importManager->didImportUserByEmail($user);

		if ($this->importManager->checkIfHasRequested($user)) {
			$this->addFlash('sonata_flash_success', 'This user has already requested');
		}

		if ($didImport['progress']) {
			$this->addFlash('sonata_flash_success', 'This user already has an import in progress');
		}

		if ($didImport['finished']) {
			$this->addFlash('sonata_flash_success', 'This User already imported his data');
		}

		if ($form->isSubmitted() && $form->isValid()) {
			$ok = false;
			try {
				$import = $this->importManager->importUserData($user, $form->getData()['email']);
				$ok = true;
			} catch (\Throwable $th) {
				$this->addFlash('warning', $th);
			}

			if ($import != "404" && $ok) {
				$this->addFlash('sonata_flash_success', 'Email has been added');
				return $this->redirectToEdit($id);
			} else {
				$this->addFlash('warning', "Email not found " . $import);
			}
		}
		return $this->renderWithExtraParams('AppBundle::admin/custom/user/user_add_to_import.html.twig', [
			'object' => $user,
			'form' => $form->createView(),
		]);
	}

	/**
	 * Change password action
	 * @param Request $request
	 * @return \Symfony\Component\HttpFoundation\Response
	 * @internal param $id
	 */
	public function changePasswordAction(Request $request, $id)
	{
		$user = $this->userManager->findById($id);

		$form = $this->createForm(UserChangePasswordType::class);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$password = $form->getData()['plainPassword'];
			if (strlen($password) >= 8) {
				$user->setPlainPassword($password);
				$this->userManager->updatePassword($user);
				$this->userManager->updateUser($user);

				$this->userLogManager->createUserLog($user->getId(), User::class, $this->getLoggedUser(), UserLog::PASSWORD_CHANGED, [], true);

				$this->addFlash('sonata_flash_success', 'Password has been changed');
			} else {
				$this->addFlash('warning', 'Password has to have min. 8 characters');
			}

			return $this->redirectToEdit($id);
		}

		return $this->renderWithExtraParams('AppBundle::admin/custom/user/change_password.html.twig', [
			'object' => $user,
			'form' => $form->createView(),
		]);
	}

	public function banUserAction(Request $request, $id)
	{
		return $this->deleteUserAction($request, $id, true);
	}

	public function deleteUserAction(Request $request, $id, $ban = null)
	{
		$user = $this->userManager->findById($id);

		$form = $this->createForm(UserDeleteType::class, ['email' => $user->getEmail()]);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$data = $form->getData();
			if ($data['sendMessageUser']) {
				$this->userManager->sendEmailMessageUser($user, $data['messageUser']);
			}

			if ($data['sendMessageReporter']) {
				$entityReports = $this->reportManager->getEntityReport(User::class, $user->getId());
				if (count($entityReports) > 0) {
					foreach ($entityReports as $report) {
						$this->userManager->sendEmailMessageUser($report->getAuthor(), $data['messageReporter']);
					}
				}
			}
			if ($data['deleteContent']) {
				$this->userManager->deleteUserHard($user);

				$this->addFlash('sonata_flash_success', 'User deleted!');

				$this->userLogManager->createUserLog(null, User::class, $this->getLoggedUser(), UserLog::USER_DELETED, ['removeUser' => $user->getEmail()], false);

				return $this->redirectToList();
			} else {
				$this->userManager->deleteUserSoft($user);
				if (!is_null($ban)) {
					$this->addFlash('sonata_flash_success', 'User is banned and User content is anonymize!');
					$this->userLogManager->createUserLog($user->getId(), User::class, $this->getLoggedUser(), UserLog::USER_BANNED, [], true);
				} else {
					$this->addFlash('sonata_flash_success', 'User content is anonymize!');
					$this->userLogManager->createUserLog($user->getId(), User::class, $this->getLoggedUser(), UserLog::USER_ANONYMIZED, [], true);
				}

				return $this->redirectToEdit($id);
			}
		}

		return $this->renderWithExtraParams('AppBundle::admin/custom/user/delete_user_form.html.twig', [
			'object' => $user,
			'form' => $form->createView(),
			'ban' => $ban
		]);
	}

	/**
	 * Show history
	 *
	 * @param Request $request
	 * @param $id
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function userHistoryAction(Request $request, $id)
	{
		$userLogs = $this->userLogManager->getUserLogForEntity($id, User::class);

		return $this->renderWithExtraParams('AppBundle::admin/custom/history.html.twig', [
			'logs' => $userLogs
		]);
	}

	/**
	 * Send urgent message all users
	 *
	 * @param Request $request
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function sendUrgentMessageAction(Request $request)
	{
		$form = $this->createForm(UserUrgentMessageType::class);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$data = $form->getData();

			if ($data['onPlatform']) {
				$this->userManager->sendPlatformMessageAllUsers($data['message']);
			}
			if ($data['onNotification']) {
				$this->userManager->sendEmailMessageAllUsers($data['message']);
			}

			$this->addFlash(
				'sonata_flash_success',
				'<div>This message has been sent!</div><p>Depending on the current load and number of recipients, it can take up to 48 hours till all members have been informed.</p>'
			);

			return $this->redirectToList();
		}

		return $this->renderWithExtraParams('AppBundle::admin/custom/user/user_urgent_message_form.html.twig', [
			'form' => $form->createView()
		]);
	}

	/**
	 * Send user message
	 *
	 * @param Request $request
	 * @param int $id
	 * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 */
	public function sendUserMessageAction(Request $request, int $id)
	{
		$user = $this->userManager->findById($id);

		$faqs = $this->faqManager->getAllQuestions();

		$form = $this->createForm(UserMessageType::class, ['email' => $user->getEmail(), 'faqs' => $faqs]);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$data = $form->getData();

			$faq = $this->faqManager->findById($data['faq']);

			$additions = [
				'faq' => $faq
			];

			if ($data['onEmail']) {
				$this->userManager->sendEmailMessageUser($user, $data['message'], $additions);
			}
			if ($data['onPlatform']) {
				$this->userManager->sendPlatformMessageUser($user, $data['message'], $additions);
			}

			$this->addFlash('sonata_flash_success', 'Message sent');

			return $this->redirectToEdit($id);
		}

		return $this->renderWithExtraParams('AppBundle::admin/custom/user/user_message_form.html.twig', [
			'object' => $user,
			'form' => $form->createView()
		]);
	}

	/**
	 * List of users threads and comments
	 *
	 * @param Request $request
	 * @param int $id
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function userCommentsAction(Request $request, int $id)
	{
		$user = $this->userManager->findById($id);

		$page = $request->get('page', 1);
		$limit = $request->get('limit', 12);

		$query = $this->commentManager->getQueryUserComments($user);

		$pager = new Pager();
		$pager->setQuery(new ProxyQuery($query));
		$pager->setMaxPerPage($limit);
		$pager->setPage($page);
		$pager->init();

		return $this->renderWithExtraParams('AppBundle::admin/custom/comments/list_comments.html.twig', [
			'userComments' => $pager->getResults(),
			'pager' => $pager,
			'id' => $id
		]);
	}

	/**
	 * Delete comment
	 *
	 * @param Request $request
	 * @return RedirectResponse
	 */
	public function deleteCommentAction(Request $request)
	{
		$commentId = $request->get('id');
		$comment = $this->commentManager->getCommentById($commentId);
		$authorId = $comment->getAuthor()->getId();

		$this->threadManager->deleteComment($comment);

		$this->addFlash('sonata_flash_success', 'Comment deleted!');

		return $this->redirectToEdit($authorId);
	}

	/**
	 * Get user files
	 *
	 * @param Request $request
	 * @param int $id
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function userFilesAction(Request $request, int $id)
	{
		$page = 1;
		$limit = 12;
		if ($request->get('page')) $page = $request->get('page');

		$query = $this->fileManager->getQueryUserFiles($id);

		$pager = new Pager();
		$pager->setQuery(new ProxyQuery($query));
		$pager->setMaxPerPage($limit);
		$pager->setPage($page);
		$pager->init();

		if ($request->get('idx')) {
			foreach ($request->get('idx') as $value) {
				$this->fileManager->deleteFile($value);
			}
		}

		return $this->renderWithExtraParams('AppBundle::admin/custom/attachments/list_attachments.html.twig', [
			'userFiles' => $pager->getResults(),
			'pager' => $pager,
			'id' => $id
		]);
	}

	/**
	 * Delete file
	 *
	 * @param Request $request
	 * @return RedirectResponse
	 */
	public function deleteFileAction(Request $request)
	{
		$fileId = $request->get('id');

		$file = $this->fileManager->getFile($fileId);
		$authorId = $file->getCreatedById();

		$this->fileManager->deleteFile($fileId);

		$this->addFlash('sonata_flash_success', 'File deleted!');

		return $this->redirectToEdit($authorId);
	}

	/**
	 * @param $id
	 * @return RedirectResponse
	 */
	public function makeSupportAdminAction($id)
	{
		$this->canUserAccess($id);

		$user = $this->userManager->findById($id);

		$role = self::ROLE_SUPER_ADMIM;

		if ($user->hasRole($role)) {
			$user->removeRole($role);
			$this->addFlash('sonata_flash_info', 'You removed '  . $user->getFullName() . ' rights as support <strong>admin</strong>!');
		} else {
			$user->addRole($role);
			$this->addFlash('sonata_flash_info', 'You added ' . $user->getFullName() . ' rights as support <strong>admin</strong>!');
		}

		$this->userManager->updateUser($user);


		return $this->redirectToEdit($id);
	}

	/**
	 * @param $id
	 * @return RedirectResponse
	 */
	public function makeSupportMemberAction($id)
	{
		$this->canUserAccess($id);

		$user = $this->userManager->findById($id);

		$role = self::ROLE_SUPPORT_MEMBER;

		if ($user->hasRole($role)) {
			$user->removeRole($role);
			$this->addFlash('sonata_flash_info', 'You removed '  . $user->getFullName() . ' rights as support <strong>member</strong>!');
		} else {
			$user->addRole($role);
			$this->addFlash('sonata_flash_info', 'You added ' . $user->getFullName() . ' rights as support <strong>member</strong>!');
		}

		$this->userManager->updateUser($user);

		return $this->redirectToEdit($id);
	}

	private function canUserAccess($id)
	{
		if ($this->getLoggedUser()->hasRole(self::ROLE_SUPER_ADMIM)) {
			$this->addFlash('sonata_flash_warning', 'You are not admin!');

			return $this->redirectToEdit($id);
		}

		return true;
	}

	/**
	 * @param Request $request
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function listSupportMembersAction(Request $request)
	{
		$page = 1;
		$limit = 12;
		if ($request->get('page')) $page = $request->get('page');

		$roles = [self::ROLE_SUPER_ADMIM, self::ROLE_SUPPORT_MEMBER];

		$query = $this->userManager->getQueryUserByRoles($roles);

		$pager = new Pager();
		$pager->setQuery(new ProxyQuery($query));
		$pager->setMaxPerPage($limit);
		$pager->setPage($page);
		$pager->init();

		return $this->renderWithExtraParams('AppBundle::admin/custom/user/list.html.twig', [
			'users' => $pager->getResults(),
			'pager' => $pager,
		]);
	}

	/**
	 * @param Request $request
	 * @param int $id
	 * @return RedirectResponse
	 */
	public function toggleRevenueAllowedAction(Request $request, int $id)
	{
		$user = $this->userManager->findById($id);

		$currentRevenueState = $user->isRevenueAllowed();
		$user->setRevenueAllowed(!$currentRevenueState);

		// If new revenue state is false and user has active revenue
		if ($user->isRevenueActive() && $currentRevenueState) {
			$user->setRevenueActive(false);
		}

		$this->userManager->updateUser($user);

		return $this->redirectToEdit($id);
	}


	/**
	 * Show action.
	 *
	 * @param int|string|null $id
	 *
	 * @throws NotFoundHttpException If the object does not exist
	 * @throws AccessDeniedException If access is not granted
	 *
	 * @return Response
	 */
	public function showAction($id = null)
	{
		$request = $this->getRequest();
		$id = $request->get($this->admin->getIdParameter());

		$object = $this->admin->getObject($id);

		if (!$object) {
			throw $this->createNotFoundException(sprintf('unable to find the object with id: %s', $id));
		}

		$this->admin->checkAccess('show', $object);

		$preResponse = $this->preShow($request, $object);
		if (null !== $preResponse) {
			return $preResponse;
		}

		$this->admin->setSubject($object);

		$exportData = $this->exportManager->userExportStatusDetailed($object);

		$object->exportData = $exportData;


		return $this->renderWithExtraParams($this->admin->getTemplate('show'), [
			'action' => 'show',
			'object' => $object,
			'elements' => $this->admin->getShow(),
			'exportData' => $exportData
		], null);
	}
}
