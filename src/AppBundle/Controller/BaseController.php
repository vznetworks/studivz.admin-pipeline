<?php


namespace AppBundle\Controller;

use AppBundle\Doctrine\UserLogManager;
use StudiVZ\Core\Doctrine\UserManager;
use Symfony\Component\HttpFoundation\Request;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

class BaseController extends CRUDController
{
	/**
	 * @var UserLogManager
	 */
	protected $userLogManager;

	/**
	 * @var UserManager
	 */
	protected $userManager;

	/**
	 * @var TokenStorageInterface
	 */
	protected $tokenStorage;

	public function __construct(
		UserManager $userManager,
		UserLogManager $userLogManager,
		TokenStorageInterface $tokenStorage
	) {
		$this->userManager = $userManager;
		$this->userLogManager = $userLogManager;
		$this->tokenStorage = $tokenStorage;
	}

	/**
	 * Get logged user
	 *
	 * @return mixed
	 */
	protected function getLoggedUser()
	{
		return $this->tokenStorage->getToken()->getUser();
	}

	/**
	 * @param int $id
	 * @return RedirectResponse
	 */
	protected function redirectToEdit(int $id)
	{
		return new RedirectResponse($this->admin->generateUrl('edit', ['filter' => $this->admin->getFilterParameters(), 'id' => $id]));
	}

}
