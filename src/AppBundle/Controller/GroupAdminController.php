<?php

namespace AppBundle\Controller;

use AppBundle\Doctrine\UserLogManager;
use AppBundle\Entity\UserLog;
use AppBundle\Form\Group\GroupChangeOwnerType;
use AppBundle\Form\Group\GroupDeleteType;
use AppBundle\Form\UserMessageType;
use StudiVZ\Core\Doctrine\FaqManager;
use StudiVZ\Core\Doctrine\GroupManager;
use StudiVZ\Core\Doctrine\ReportManager;
use StudiVZ\Core\Doctrine\UserManager;
use StudiVZ\Core\Entity\Group;
use StudiVZ\Core\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class GroupAdminController extends BaseController
{

	/**
	 * @var GroupManager
	 */
	private $groupManager;

	/**
	 * @var ReportManager
	 */
	private $reportManager;

	/**
	 * @var FaqManager
	 */
	private $faqManager;

	public function __construct(
		UserManager $userManager,
		UserLogManager $userLogManager,
		TokenStorageInterface $tokenStorage,
		GroupManager $groupManager,
		ReportManager $reportManager,
		FaqManager $faqManager
	){
		parent::__construct($userManager, $userLogManager, $tokenStorage);
		$this->groupManager = $groupManager;
		$this->reportManager = $reportManager;
		$this->faqManager = $faqManager;
	}

	/**
	 * Delete group
	 *
	 * @param Request $request
	 * @param $id
	 * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 */
	public function deleteGroupAction(Request $request, $id)
	{
		$group = $this->groupManager->findGroupById($id);
		$groupMembers = $this->groupManager->getGroupAllAdmins($group->getId(), true);
		$email = '';
		$owner = false;
		if(count($groupMembers) > 0) {
			$user = $this->userManager->findById($groupMembers[0]->getUserId());
			$owner = true;
			$email = $user->getEmail();
		} else {
			$this->addFlash('sonata_flash_warning', "Group doesn't has owner!");
		}

		$form = $this->createForm(GroupDeleteType::class, ['email' => $email]);

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$data = $form->getData();
			if($data['sendMessageOwner'] && $owner) {
				$this->userManager->sendEmailMessageUser($user, $data['messageUser']);
			}

			if($data['sendMessageReporter']) {
				$entityReports = $this->reportManager->getEntityReport(Group::class, $group->getId());
				if(count($entityReports) > 0) {
					foreach ($entityReports as $report) {
						$this->userManager->sendEmailMessageUser($report->getAuthor(), $data['messageReporter']);
					}
				}
			}
			if($data['deleteOwner'] && $owner) {
				$this->userManager->deleteUserSoft($user);
				$this->userManager->deleteUserHard($user);
			}

			$this->groupManager->deleteGroup($group->getId());

			$this->addFlash('sonata_flash_success', 'Group deleted!');

			$this->userLogManager->createUserLog(null, Group::class, $this->getLoggedUser(), UserLog::GROUP_DELETED, ['groupName' => $group->getTitle()], true);

			return $this->redirectToList();
		}

		return $this->renderWithExtraParams('AppBundle::admin/custom/group/delete_group_form.html.twig', [
			'object' => $group,
			'form' => $form->createView(),
		]);
		
	}

	/**
	 * Show group history
	 *
	 * @param Request $request
	 * @param $id
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function groupHistoryAction(Request $request, $id)
	{
		$userLogs = $this->userLogManager->getUserLogForEntity($id, Group::class);

		return $this->renderWithExtraParams('AppBundle::admin/custom/history.html.twig', [
			'logs' => $userLogs
		]);
	}

	/**
	 * Change owner for group
	 *
	 * @param Request $request
	 * @param $id
	 * @return \Symfony\Component\HttpFoundation\Response
	 */
	public function changeOwnerAction(Request $request, $id)
	{
		$adminUser = $this->admin->getConfigurationPool()->getContainer()->get('sonata.admin.pool')->getAdminByClass(User::class);
		$adminGroup = $this->admin->getConfigurationPool()->getContainer()->get('sonata.admin.pool')->getAdminByClass(Group::class);
		$modelManager = $this->admin->getModelManager()->getNewFieldDescriptionInstance(Group::class, 'owner');

		$form = $this->createForm(GroupChangeOwnerType::class, [ 'adminUser' => $adminUser, 'adminGroup' => $adminGroup, 'modelManager' => $modelManager]);

		$form->handleRequest($request);

		if($form->isSubmitted() && $form->isValid()) {
			die();
		}


		return $this->renderWithExtraParams('AppBundle::admin/custom/group/group_change_owner.html.twig', [
			'form' => $form->createView(),
		]);
	}

	/**
	 * Send user message
	 *
	 * @param Request $request
	 * @param int $id
	 * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
	 */
	public function sendOwnerMessageAction(Request $request, int $id)
	{
		$groupMembers = $this->groupManager->getGroupAllAdmins($id, true);
		$member = $groupMembers[0];
		$user = $this->userManager->findById($member->getUserId());

		$faqs = $this->faqManager->getAllQuestions();

		$form = $this->createForm(UserMessageType::class, ['email' => $user->getEmail(), 'faqs' => $faqs]);

		$form->handleRequest($request);

		if($form->isSubmitted() && $form->isValid()) {
			$data = $form->getData();

			$faq = $this->faqManager->findById($data['faq']);

			$additions = [
				'faq' => $faq
			];

			if($data['onEmail']) {
				$this->userManager->sendEmailMessageUser($user, $data['message'], $additions);
			}
			if($data['onPlatform']) {
				$this->userManager->sendPlatformMessageUser($user, $data['message'], $additions);
			}

			$this->addFlash('sonata_flash_success', 'Message sent');

			return $this->redirectToEdit($id);
		}

		return $this->renderWithExtraParams('AppBundle::admin/custom/user/user_message_form.html.twig', [
			'object' => $user,
			'form' => $form->createView()
		]);
	}
}