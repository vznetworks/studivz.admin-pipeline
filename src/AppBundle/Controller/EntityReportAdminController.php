<?php

namespace AppBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormRenderer;

class EntityReportAdminController extends CRUDController
{
    /**
     * @param $id
     */
    public function showAction($id = null)
    {
        $request = $this->getRequest();
        $id = $request->get($this->admin->getIdParameter());

        $object = $this->admin->getObject($id);

        if (!$object) {
            throw $this->createNotFoundException(sprintf('unable to find the object with id: %s', $id));
        }

        $this->admin->checkAccess('show', $object);

        $preResponse = $this->preShow($request, $object);
        if (null !== $preResponse) {
            return $preResponse;
        }

        $this->admin->setSubject($object);

        $pool = $this->admin->getConfigurationPool();
        $entityAdmin = $pool->getAdminByClass($object->getReportEntity());

        $url = $entityAdmin->generateUrl('show', ['id' => $object->getReportEntityId()]);

        $object->reportEntityUrl = $url;

        return $this->renderWithExtraParams($this->admin->getTemplate('show'), [
            'action' => 'show',
            'object' => $object,
            'elements' => $this->admin->getShow(),
        ], null);
    }

    /**
     * List action.
     *
     * @throws AccessDeniedException If access is not granted
     *
     * @return Response
     */
    public function listAction()
    {
        $request = $this->getRequest();

        $this->admin->checkAccess('list');

        $preResponse = $this->preList($request);
        if (null !== $preResponse) {
            return $preResponse;
        }

        if ($listMode = $request->get('_list_mode')) {
            $this->admin->setListMode($listMode);
        }

        $datagrid = $this->admin->getDatagrid();

        $dataGridResults = $datagrid->getResults();

        $pool = $this->admin->getConfigurationPool();

        // Inject new field into every result entry
        foreach ($dataGridResults as $object) {
            $entityAdmin = $pool->getAdminByClass($object->getReportEntity());

            $url = $entityAdmin->generateUrl('show', ['id' => $object->getReportEntityId()]);

            $object->reportEntityUrl = $url;
        }

        $formView = $datagrid->getForm()->createView();
        //dump($this->admin->getTemplate('list'));

        // set the theme for the current Admin Form
        $this->get('twig')->getRuntime(FormRenderer::class)->setTheme($formView, $this->admin->getFilterTheme());

        return $this->renderWithExtraParams($this->admin->getTemplate('list'), [
            'action' => 'list',
            'form' => $formView,
            'datagrid' => $datagrid,
            'csrf_token' => $this->getCsrfToken('sonata.batch'),
            'export_formats' => $this->has('sonata.admin.admin_exporter') ?
                $this->get('sonata.admin.admin_exporter')->getAvailableFormats($this->admin) :
                $this->admin->getExportFormats(),
        ], null);
    }
}