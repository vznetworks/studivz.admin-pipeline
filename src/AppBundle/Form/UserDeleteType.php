<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class UserDeleteType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('sendMessageUser', CheckboxType::class, [
				'label' => 'Send Message to user',
				'required' => false
			])
			->add('email', null, ['disabled' => true])
			->add('messageUser', TextareaType::class, ['required' => false])
			->add('sendMessageReporter', CheckboxType::class, [
				'label' => 'Send Message to reporter',
				'required' => false
			])
			->add('messageReporter', TextareaType::class, ['required' => false])
			->add('deleteContent', CheckboxType::class, [
					'label' =>  'Delete all content of this user!',
					'required' => false
				]
			)
		;
	}
}
