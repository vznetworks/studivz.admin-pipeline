<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class UserUrgentMessageType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('message', TextareaType::class, [
				'required' => true,
				'attr' => ['class' => 'content-message']
			])
			->add('onPlatform', CheckboxType::class, [
				'label' => 'by platform',
				'required' => false
			])
			->add('onNotification', CheckboxType::class, [
					'label' =>  'by email',
					'required' => false
				]
			)
		;
	}
}