<?php

namespace AppBundle\Form\Group;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class GroupDeleteType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('sendMessageOwner', CheckboxType::class, [
				'label' => 'Send Message to owner',
				'required' => false
			])
			->add('email', null, ['disabled' => true])
			->add('messageUser', TextareaType::class, ['required' => false])
			->add('sendMessageReporter', CheckboxType::class, [
				'label' => 'Send Message to reporter',
				'required' => false
			])
			->add('messageReporter', TextareaType::class, ['required' => false])
			->add('deleteOwner', CheckboxType::class, [
					'label' =>  'Delete Owner also!',
					'required' => false
				]
			)
		;
	}
}
