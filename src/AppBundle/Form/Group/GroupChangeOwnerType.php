<?php

namespace AppBundle\Form\Group;

use Doctrine\ORM\Mapping\ClassMetadataInfo;
use StudiVZ\Core\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class GroupChangeOwnerType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$data = $options['data'];
		$adminUser = $data['adminUser'];
		$adminGroup = $data['adminGroup'];
		$modelManager = $data['modelManager'];
		$fieldDescription = $modelManager;


		$fieldDescription->setAdmin($adminGroup);
		$fieldDescription->setAssociationAdmin($adminUser);
		$fieldDescription->setAssociationMapping([
			'fieldName' => 'owner',
			'type' => ClassMetadataInfo::MANY_TO_ONE,
			'targetEntity' => 'StudiVZ\Code\Entity\User'
		]);

		$ownerField = $builder->create('owner', 'sonata_type_model_autocomplete', [
			'property' => 'email',
			'sonata_field_description' => $fieldDescription,
			'class' => $adminUser,
			'model_manager' => $adminUser->getModelManager(),
			'label' => 'Owner',
			'mapped' => false,
			'required' => false
		]);


		$builder
			->add($ownerField, 'sonata_type_model_autocomplete')
		;

		$data['adminGroup']->addFormFieldDescription('owner', $fieldDescription);
	}
}
