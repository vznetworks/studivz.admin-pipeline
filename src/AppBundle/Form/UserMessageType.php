<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class UserMessageType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$faqs = $options['data']['faqs']->response;

		$choices = [];
		foreach ($faqs as $value) {
			$choices[$value->getId()] = $value->getId() . ' => ' . $value->getTitle();
		}

		$builder
			->add('onPlatform', CheckboxType::class, [
				'label' => 'By platform',
				'required' => false
			])
			->add('onEmail', CheckboxType::class, [
					'label' =>  'By email',
					'required' => false
				]
			)
			->add('email', null, ['disabled' => true])
			->add('message', TextareaType::class, [
				'required' => true,
				'attr' => ['class' => 'content-message']
			])
			->add('faq', 'choice', [
				'choices' => array_flip($choices),
				'required' => false,
			])
		;
	}
}