<?php

namespace AppBundle\Twig;

use StudiVZ\Core\Doctrine\EventManager;
use StudiVZ\Core\Doctrine\GroupManager;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    /** @var GroupManager */
    private $groupManager;

    /** @var EventManager */
    private $eventManager;

    /**
     * @param GroupManager $groupManager
     * @param EventManager $eventManager
     */
    public function __construct(GroupManager $groupManager, EventManager $eventManager)
    {
        $this->groupManager = $groupManager;
        $this->eventManager = $eventManager;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('commentWebAppPath', [$this, 'getCommentWebAppPath']),
        ];
    }

    /**
     * Create url to comment in web app.
     *
     * @param array $commentData
     * @param string $webappBaseUrl
     *
     * @return string
     */
    public function getCommentWebAppPath(array $commentData, string $webappBaseUrl)
    {
        $path = "";

        switch ($commentData['threadEntity']) {
            case "StudiVZ\Core\Entity\ForumThread":
                $path = $this->getForumThreadCommentPath($commentData['forumThreadSlug'], $commentData['forumGroupId']);
                break;
            case "StudiVZ\Core\Entity\Event":
                $path = $this->getEventCommentPath($commentData['threadEntityId']);
                break;
            case "StudiVZ\Core\Entity\BlogPost":
                $path = $this->getBlogPostCommentPath($commentData['postSlug'], $commentData['groupId']);
                break;
        }

        return $webappBaseUrl . $path;
    }

    /**
     * Return webapp path to forum thread comment
     *
     * @param string $threadSlug
     * @param int $groupId
     *
     * @return string
     */
    private function getForumThreadCommentPath(string $threadSlug, int $groupId)
    {
        $group = $this->groupManager->findGroupById($groupId);

        return "/groups/" . $group->getSlug() . "/forum/" . $threadSlug;
    }

    /**
     * Return webapp path to event comment
     *
     * @param int $eventId
     *
     * @return string
     */
    private function getEventCommentPath(int $eventId)
    {
        $event = $this->eventManager->findEventById($eventId);

        return "/event/" . $event->getUuid();
    }

    /**
     * Return webapp path to blog post comment
     *
     * @param string $postSlug
     * @param int $groupId
     *
     * @return string
     */
    private function getBlogPostCommentPath(string $postSlug, int $groupId)
    {
        $group = $this->groupManager->findGroupById($groupId);

        return "/groups/" . $group->getSlug() . "/blog/" . $postSlug;
    }
}
