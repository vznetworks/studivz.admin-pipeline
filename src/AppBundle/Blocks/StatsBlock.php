<?php

namespace AppBundle\Blocks;

use Doctrine\ORM\EntityManager;
use Sonata\BlockBundle\Block\BaseBlockService;
use Sonata\BlockBundle\Block\BlockServiceInterface;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Response;

use Sonata\AdminBundle\Admin\Pool;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\BlockBundle\Block\Service\AbstractBlockService;

class StatsBlock extends AbstractBlockService
{
	protected $pool;

	/**
	 * @param string $name
	 */
	public function __construct($name, EngineInterface $templating, Pool $pool)
	{
		parent::__construct($name, $templating);

		$this->pool = $pool;
	}

	/**
	 * @return object
	 */
	private function getGroupManager()
	{
		return $this->pool->getContainer()->get('studivz_core.group.manager');
	}

	/**
	 * @return object
	 */
	private function getEventManager()
	{
		return $this->pool->getContainer()->get('studivz_core.event.manager');
	}

	/**
	 * @return object
	 */
	private function getUserManager()
	{
		return $this->pool->getContainer()->get('studivz_core.user.manager');
	}

	/**
	 * @return object
	 */
	private function getPhotoManager()
	{
		return $this->pool->getContainer()->get('studivz_core.photo.manager');
	}

	/**
	 * @return object
	 */
	private function getCommentManager()
	{
		return $this->pool->getContainer()->get('StudiVZ\Core\Doctrine\CommentManager');
	}

	public function execute(BlockContextInterface $blockContext, Response $response = null)
	{
//		dump($this->getGroupManager()->getGroupsCount());
//		die();

		return $this->renderResponse('AppBundle::admin/blocks/admin_block_studivz_user.html.twig', array(
//			'countPhotos' => $this->getPhotoManager()->getPhotosCount(),
			'countComments' => $this->getCommentManager()->getCommentsCount(),
			'countEvents' => $this->getEventManager()->getEventsCount(),
			'countUsers' => $this->getUserManager()->getUsersCount(),
			'countGroup' => $this->getGroupManager()->getGroupsCount(),
			'block'     => $blockContext->getBlock(),
		), $response);
	}
}
