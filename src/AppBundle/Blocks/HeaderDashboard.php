<?php

namespace AppBundle\Blocks;

use Doctrine\ORM\EntityManager;
use Sonata\BlockBundle\Block\BaseBlockService;
use Sonata\BlockBundle\Block\BlockServiceInterface;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Response;

use Sonata\AdminBundle\Admin\Pool;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\BlockBundle\Block\Service\AbstractBlockService;

class HeaderDashboard extends AbstractBlockService
{
	protected $pool;
	protected $tokenStorage;

	/**
	 * @param string $name
	 */
	public function __construct($name, EngineInterface $templating, Pool $pool)
	{
		parent::__construct($name, $templating);

		$this->pool = $pool;
	}

	public function execute(BlockContextInterface $blockContext, Response $response = null)
	{
		return $this->renderResponse('AppBundle::admin/blocks/admin_block_dashboard_header.html.twig', [], $response);
	}
}
