<?php

namespace AppBundle\Command;

use StudiVZ\Core\Notifications\Messages;
use SupportBundle\Support\Arrays;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Process;

/**
 * Used just for test purposes.
 */
class GenerateJWTCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('jwt:key:make')
            ->setDescription('Generates JWT private and public key.')
            ->setHelp('Generates JWT private and public key.');
    }

    /**
     * @param $service
     * @return mixed
     */
    private function resolve($service)
    {
        return $this->getContainer()->get($service);
    }

    /**
     * @var OutputInterface
     */
    private $output;

    /**
     * @var InputInterface
     */
    private $input;

    private function error($message)
    {
        $formatter = $this->getHelper('formatter');
        $errorBlock = $formatter->formatBlock([$message], 'error', true);
        $this->output->writeln($errorBlock);
    }

    private function info($message)
    {
        $formatter = $this->getHelper('formatter');
        $infoBlock = $formatter->formatBlock([$message], 'comment', true);
        $this->output->writeln($infoBlock);
    }

    private function success($message)
    {
        $formatter = $this->getHelper('formatter');
        $successBlock = $formatter->formatBlock([$message], 'info', true);
        $this->output->writeln($successBlock);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        $this->input = $input;

        $root = $this->resolve('kernel')->getRootDir();

        $fs = new Filesystem();

        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion('<comment>WARNING: This will remove all existing private and public keys in var/jwt folder.'.PHP_EOL.'Do you want to proceed?[Y/n]: </comment>', true);

        if (!$helper->ask($input, $output, $question)) {
            $this->error('Aborted.');
            return;
        }

        $path = realpath($root.'/../var/jwt');

        if (!$fs->exists($path)) {
            $fs->mkdir($path);
            $this->info('✔ Directory jwt created.');
        }

        $privateKeyFilePath = $path.'/private.pem';
        $publicKeyFilePath = $path.'/public.pem';

        if ($fs->exists($privateKeyFilePath)) {
            $fs->remove($privateKeyFilePath);
            $this->info('✔ Private key in var/jwt have been deleted.');
        }

        if ($fs->exists($publicKeyFilePath)) {
            $fs->remove($publicKeyFilePath);
            $this->info('✔ Public key in var/jwt have been deleted.');
        }

        $privateKeyCommand = "openssl genrsa -out {privateKeyFilePath} 4096";
        $process = new Process($privateKeyCommand);
        $process->run();

        $publicKeyCommand = "openssl rsa -in {$privateKeyFilePath} -pubout -outform PEM -out {$publicKeyFilePath}";
        $process = new Process($publicKeyCommand);
        $process->run();

        if (!$process->isSuccessful()) {
            $this->error('We could not generate public key. Do you have openssl installed and accessible on your machine?');
            return;
        }

        $success = true;

        if ($fs->exists($privateKeyFilePath)) {
            $this->success('✔ Created: var/jwt/private.pem');
        } else {
            $success = false;
            $this->error('✗ Error - not created: var/jwt/private.pem');
        }

        if ($fs->exists($publicKeyFilePath)) {
            $this->success('✔ Created: var/jwt/public.pem');
        } else {
            $success = false;
            $this->error('✗ Error - not created: var/jwt/public.pem');
        }

        if ($success) {
            $this->success('✔ The keys have been generated successfully.');
            $this->info('Run lexik:jwt:check-config to check if the JWT configuration is correct.');
        } else {
            $this->error('✗ Keys were not generated properly.');
        }
    }

}
