<?php

namespace AppBundle\Compiler;


use Doctrine\Common\Inflector\Inflector;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class AdminCompilerPass implements CompilerPassInterface
{
	public function process(ContainerBuilder $container)
	{
		foreach ($container->findTaggedServiceIds('sonata.admin') as $id => $attributes) {
			$definition = $container->getDefinition($id);
			$adminClassTab = \explode('\\', $definition->getClass());
			$entityName = \preg_replace('/Admin$/', '', \end($adminClassTab));

			if (strpos($definition->getClass(), 'sonata') === false) {
				$pathEntity = "StudiVZ\\Core\\Entity\\{$entityName}";
				$definition
					->clearTag('sonata.admin')
					->addTag('sonata.admin', [
						'manager_type' => 'orm',
						'label' => Inflector::ucwords(\str_replace('_', ' ', Inflector::tableize($entityName))),
					])
					->setArguments([
						null,
						$pathEntity,
						null,
					])
				;
			}
		}
	}
}
