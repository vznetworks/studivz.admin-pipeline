#!/usr/bin/env bash

PROGNAME=$(basename "$0")

if [[ $# != 3 ]]; then
    echo "Usage: $PROGNAME [aws profile] [cluster] [version]" >&2
    exit 1
fi

# https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-envvars.html
#
export AWS_PROFILE="$1"
AWS_EKS_CLUSTER="$2"

set -e
set -x
set -u

aws eks describe-cluster --name "$AWS_EKS_CLUSTER"
aws eks update-kubeconfig --name "$AWS_EKS_CLUSTER"

PROJECT_ID=$(basename -s .git "$(git config --get remote.origin.url)")
PROJECT_DIR=$(git rev-parse --show-toplevel)
PROJECT_VERSION="$3"

KUBERNETES_NAMESPACE="${PROJECT_ID//./-}"

cd "$(git rev-parse --show-toplevel)"

/bin/bash -l -c "REVISION=\"$PROJECT_VERSION\" \
    kubernetes-deploy \
        --template-dir \"$PROJECT_DIR/kubernetes/templates\" \
        --no-prune \
        --bindings \"@$PROJECT_DIR/kubernetes/$AWS_EKS_CLUSTER.yml\" \
        --binding version=\"$PROJECT_VERSION\" \
        --binding project=\"$PROJECT_ID\" \
        --binding aws_account_id=\"$(aws sts get-caller-identity | jq -r .Account)\" \
        --binding aws_region=\"$(aws configure get region)\" \
        \"$KUBERNETES_NAMESPACE\" \
        \"$(kubectl config current-context)\""
