#!/usr/bin/env bash

set -e

cd "$(git rev-parse --show-toplevel)"

PROJECT=$(basename -s .git "$(git config --get remote.origin.url)")
VERSION="$(git rev-parse --short=8 HEAD)"

set -x

AWS_REGION="$(aws configure get region)"
AWS_ACCOUNT_ID="$(aws sts get-caller-identity | jq -r .Account)"

# 104034259154.dkr.ecr.eu-west-2.amazonaws.com/studivz.api/app

eval "$(aws ecr get-login --no-include-email)"

rm -fr ./studivz.core || true

git clone git@bitbucket.org:vznetworks/studivz.core.git ./studivz.core

for DOCKERFILE in Dockerfile.*; do
    DOCKERIMAGETYPE=${DOCKERFILE/Dockerfile.}
    DOCKERIMAGE="$AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/$PROJECT/$DOCKERIMAGETYPE:$VERSION"

    docker build \
        --ssh default \
        --label project.version="$VERSION" \
        --label project.build_time="$(date)" \
        --label project.build_user="$(whoami)" \
        --label project.build_machine="$(hostname -f)" \
        --label project.build_branch="$(git rev-parse --abbrev-ref HEAD)" \
        -t "$DOCKERIMAGE" \
        -f "$DOCKERFILE" .

    docker push "$DOCKERIMAGE"
done

echo "BUILD VERSION: $VERSION"

./bin/deploy.sh default staging $VERSION