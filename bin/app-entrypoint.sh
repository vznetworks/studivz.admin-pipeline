#!/bin/sh

set -x
set -e

if [ -n  "$COMPOSER_RUN_SCRIPTS" ]; then
    for RUN_SCRIPT in $COMPOSER_RUN_SCRIPTS; do
        composer run-script "$RUN_SCRIPT"
    done
fi

/usr/local/bin/docker-php-entrypoint "$@"
